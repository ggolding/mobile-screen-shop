//Sets the default arrival and departure dates to today and tomorrow respectively
var dtToday = new Date();
var dtTomorrow = new Date();
dtTomorrow.setDate(dtToday.getDate()+1);

document.getElementById('p_amonth').value = dtToday.getMonth();
document.getElementById('p_aday').value = dtToday.getDate();
document.getElementById('p_ayear').value = dtToday.getFullYear();
document.getElementById('p_dmonth').value = dtTomorrow.getMonth();
document.getElementById('p_dday').value = dtTomorrow.getDate();
document.getElementById('p_dyear').value = dtTomorrow.getFullYear();
