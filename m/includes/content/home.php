      <!-- Main Content Area -->
      <section id="main"> 
        
        <!-- Box 1 -->
        <div class="module">
          <h3>We Come to You</h3>
          <div>
            <p>Mobile Screen Shop is a fully equipped mobile screen service that will come to you in the convenience of your home. Our products include custom window screens and door screens as well as retractable door screens.</p>
          </div>
        </div>
        
        <!-- Box 2 -->
        <div class="module">
          <h3>Specializing In:</h3>
          <div style="padding: 0;">
            <ul>
              <li>Retractable Doors</li>
              <li>Swinging Screen Doors</li>
              <li>Sliding Patio Door Screens</li>
              <li>Rescreen Doors</li>
              <li>Rescreen Windows</li>
              <li>Aluminum Security Doors</li>
            </ul>
          </div>
        </div>
        
        <!-- Box 3 -->
        <div class="module">
          <h3>Solutions for All of Your Screen Needs</h3>
          <div>
            <p>Mobile Screen Shop is proud to offer the following services which include: new screens and rescreen of doors and windows, door screens, storm doors, sliding patio screen doors, extra heavy duty sliding patio door screens, heavy duty pet screen, Phifer sun screens, SunTex, glassine patio panels, porch screens, patio screens, garage screens and garage door screens, and retractable disappearing screen doors.
            </p>
            <p>Through the years we have established ourselves as a reputable company offering quality products at competitive prices. We at Mobile Screen Shop look forward to serving you.</p>
          </div>
        </div>
        
      </section>