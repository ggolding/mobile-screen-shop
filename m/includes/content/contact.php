      <!-- Main Content Area -->
      <section id="main"> 
        
        <!-- Box 1 -->
        <div class="module">
          <h3>Contact Us</h3>
          <div style="padding: 0;">
            <ul>
              <li style="font-size: 1.2em;">Toll Free: (800) 606-7273</li>
              <li style="font-size: 1.2em;">Canyon Lake: (951) 244-0987</li>
              <li style="font-size: 1.2em;">Corona: (951) 371-6555</li>
              <li style="font-size: 1.2em;">Temecula: (951) 699-0308</li>
              <li style="font-size: 1.2em;">Riverside: (951) 689-9242</li>
            </ul>
          </div>
        </div>
        
        <!-- Box 3 -->
        <div class="module">
          <h3>Solutions for All of Your Screen Needs</h3>
          <div>
            <p>Contact Mobile Screen Shop For All Of Your Screen Needs. Certified Home Improvement Contractor - Insured - CA License 712571</p>
          </div>
        </div>
        
      </section>