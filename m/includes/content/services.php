      
      <!-- Main Content Area -->
      <section id="main"> 
        
        <!-- Box 1 -->
        <div class="module">
          <h3>Mirage Retractable Screen System</h3>
          <div>
            <img src="../images/mirage-retractable-screen-system.jpg" />
            <p>We sell the highest quality retractable on the market today installed the same day using my 30 years of installation experience. Several colors to choose from.</p>
          </div>
        </div>
        
        <!-- Box 2 -->
        <div class="module">
          <h3>Swinging Screen Doors</h3>
          <div>
            <img src="../images/swinging-screen-door.jpg" />
            <p>I use active window products screen doors because I have found their selection, quality, and quick turnaround to be the best choice. Not to mention the selection and color options. I can order any size door, including 8' tall and get them as quick as a standard size. We also have different choices for screening.</p>
          </div>
        </div>
        
        <!-- Box 3 -->
        <div class="module">
          <h3>Sliding Screen Doors</h3>
          <div>
            <img src="../images/sliding-screen-door.jpg" />
            <p>I offer two sliding doors that I fabricate on site. Both doors are a heavier grade than the doors usually put on during construction. I can make the doors on site, even 8' tall. We can add pet screen fabric to the doors also.</p>
          </div>
        </div>
        
        <!-- Box 4 -->
        <div class="module">
          <h3>Window Screens</h3>
          <div>
            <img src="../images/window-screen.jpg" />
            <p>We fabricate window screens on site, custom sized to the window specifications. We also offer pet screen or solar suntex screens to tame the sun.</p>
          </div>
        </div>
        
      </section>