  <!-- Sidebar Begin -->
  <aside id="sidebar">
    <div class="sidebar-scroll"> 
      
      <!-- Main Navigation -->
      <?php
        $active[$current] = "class=active";
        $activetwo[$current] = 'active' ;
      ?>
      
      <nav>
        <ul>
          <li <?=$active[1];?>><a href="<?=$home;?>" title="Home">HOME</a></li>
          <li <?=$active[2];?>><a href="<?=$services;?>" title="Services">SERVICES</a></li>
          <li <?=$active[3];?>><a href="<?=$contact;?>" title="Contact Us">CONTACT US</a></li>
        </ul>
      </nav>
      
      <!-- Contact Information -->
      <section id="contact">
        <h2>Get in Touch</h2>
        <ul>
          <li class="web"><a href="http://mobilescreenshop.com/?redirect=false" title="Website">mobilescreenshop.com<span>Full Site</span></a></li>
          <li class="email"><a href="mailto:info@mobilescreenshop.com" title="E-Mail">info@mobilescreenshop.com</a></li>
          <li class="phone"><a href="tel:800-606-7273" title="Phone">800-606-7273<span>Press to Call</span></a></li>
        </ul>
      </section>
    </div>
  </aside>
  <!-- Sidebar End --> 