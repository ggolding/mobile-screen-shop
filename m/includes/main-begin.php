  <!-- Main Content Begin -->
  <section id="content">
    <div class="content-scroll"> 
      
      <!-- Sidebar Toggle & Tabbed Navigation -->
      <header>
        <div class="controls"> <a title="Toggle Sidebar"><b>MENU</b></a> </div>
        <ul>
          <li class="home <?=$activetwo[1];?>"><a href="<?=$home;?>" title="Home">Home</a></li>
          <li class="services <?=$activetwo[2];?>"><a href="<?=$services;?>" title="Services">Services</a></li>
          <li class="call <?=$activetwo[3];?>"><a href="<?=$contact;?>" title="Contact Us">Contact</a></li>
        </ul>
      </header>
      
      <!-- Logo & Slideshow Controls -->
      <section id="branding"> 
        <a href="tel:800-606-7273" title="Back to Homepage"><img src="<?=$a;?>images/main-title.jpg" alt="Logo" /></a>
      </section>
      
      <!-- Introduction & Slideshow Contents -->
      <section id="slideshow">
<?php include($includes . 'banner/' . $thisPageContent); ?>
        
        <h1 class="button"><?=$pagetitle;?></h1>
        
        <br />
      </section>