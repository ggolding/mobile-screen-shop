<?php 
	$a = '../';
	$includes = $a . 'includes/' ;
	$current = 3;
	$thisPageContent = 'contact.php';
	$pagetitle = 'CONTACT US';

	  include ($includes . '0.seo.php');
	
	$SEOTitle = $Thome;
	$SEOKeywords = $Khome;
	$SEODescription = $Dhome;
	
	  include ($includes . '0.sitelinks.php');
	  include ($includes . '1.doctype.php');
?>


<head>
<?php include ($includes . '2.head.php'); ?>

  <title><?=$SEOTitle?></title>
		
  <meta name="keywords" content="<?=$SEOKeywords?>">	
  <meta name="description" content="<?=$SEODescription?>">
  
</head>

<body>
<div id="wrapper"> 
  
<?php 
  include($includes . 'sidebar.php'); 
  include($includes . 'main-begin.php'); 
  include($includes . 'content/' . $thisPageContent); 
  include($includes . 'footer.php'); 
?>
      

  
</div>

<?php include ($includes . 'jquery.php'); ?>

</body>
</html>