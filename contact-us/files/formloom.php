<?php
////////////////////////////////////////////////////////////////
//
// formloom.php
// FormLoom
// VERSION 1.3.2
// 02-04-10 09.06.56 AM
// Send questions to: support@yabdab.com
//
// This script uses phpMailer 5, a free GPL licensed solution
// 
////////////////////////////////////////////////////////////////


//=====================================================//
// ! CONFIGURE
//=====================================================//

$allowedFileTypes = "doc|xls|pdf|jpg|jpeg|png|gif|zip|rar|gz";
$today = date("F j, Y, g:i a T");
$fields = $_POST;
$filesFolder = 'files';
$sendToEmail = "info@mobilescreenshop.com";
$sendToName = "Mobile Screen Shop";
$sendUsingToAddress = 'false';
$makeSenderReplyToAddress = 'false';
$cc = "";
$bcc = "";
$fromEmail = $_POST['email'];
$fromName = $_POST['name'];
$wordwrap = "50";
$subject = $_POST['how'];
$sendreceipt = "true"; // LEAVE BLANK FOR NO RECEIPT, MAKE true TO SEND RECEIPT
$use_captcha = "false";
$email_bad_array = "\r|\n|to:|cc:|bcc:";
$required_fields = "name,email,information,";
$isAjax = $_POST['ajax'];
$okButton = "<img src=\"./".$filesFolder."/button_ok.png\" alt=\"OK\" id=\"block-button\" class=\"hud-button\" />";
$page = $_SERVER['HTTP_REFERER'];
$useAjax = "true";
$successUrl = '';
$errorUrl = '';
$reCaptchaPublicKey = '6LfELgQAAAAAAO8nU9W5zi7QOfCClf1FPA7NJJ84';
$reCaptchaPrivateKey = '6LfELgQAAAAAAJTjfzclJ9YtE5ixDmRSYSE7gB7R';
$maxAttachmentSize = 10;
$maxAttachmentSizeInBytes = $maxAttachmentSize * 1024; // Covert KB to bytes value
$usePHP4 = '';
$usePlainText = '';

// SMTP Server
$useSmtpServer = '';
$smtpServerName = '';
$smtpServerPort = '%smtp-server-port%';
$smtpUsername = '';
$smtpPassword = '';
$smtpSecure = '%smtp-secure%';

// Ajax Messages
$successMessage = '<h1>Success</h1>Your Form was submitted Successfully';
$successLongMessage = '';
$captchaFailed = '<h1>Alert!</h1> Security Test Failed';
$fileTooBig = '<h1>Alert!</h1> Attachment Size Too Big';
$errorMessage = '<h1>Alert!</h1>There was an error processing your form.';
$emptyRequiredFieldAlert = '<h1>Alert!</h1> Missing Required Input';
$invalidEmailAlert = '<h1>Alert!</h1> Email Format is Invalid';

// Email template vars
$platform = getPlatform();
$browser = getBrowser();
$fl_date = date("r");
$fl_ip = $_SERVER['REMOTE_ADDR'];
$fl_browser = $platform. " ".$browser;
$fl_page = $_SERVER['REQUEST_URI'];
$receipt_text = stripslashes('');
$emailTemplate = '';

// MySQL Prefs
$saveToMySQL = "";
$mysqlHostName = "";
$mysqlDatabaseName = "";
$mysqlUserName = "";
$mysqlPassword = "";

//=====================================================//
// ! DO WE HAVE WHAT WE NEED?
//=====================================================//

if (!$useAjax){
	if(!$errorUrl || !$successUrl){
		echo "ERROR! - Please specify Error and Success Page"; die;
	}
}
 
//=====================================================//
// ! PHP5 CHECK
//=====================================================//



$php_version = phpversion();

 if( $php_version < 5 && !$usePHP4 ){
 
 if ($isAjax || $useAjax){
		$eval= "ydfl.blockUI({ message: '<img src=\"./".$filesFolder."/alert.png\" alt=\"Alert\" /><br /><h1>Alert</h1><p>FormLoom Requires PHP5.<br />The server this script is hosted on is using PHP Version $php_version.<br />Upgrade your server to PHP5 and try again.</p>".$okButton."' });
		ydfl('#block-button', '.blockMsg').click(function() { 
		ydfl.unblockUI(); 
		Recaptcha.reload(); });";
		taconite($eval, "", "");
	}else{
		header("Location: $errorUrl?error=FormLoom+Requires+PHP5");
	}

die;

}




//=====================================================//
// ! REQUIREMENTS
//=====================================================//

if($usePHP4){
	require_once("class.phpmailer.php4.php"); // Get needed phpmailer Class.
	require_once("class.smtp.php4.php"); // Get needed phpmailer SMTP Class.
}else{
	require_once("class.phpmailer.php"); // Get needed phpmailer Class.
	require_once("class.smtp.php"); // Get needed phpmailer SMTP Class.
}

require_once('recaptchalib.php'); // include reCAPTCHA lib

//=====================================================//
// ! HELPER FUNCTIONS
//=====================================================//
/// TACONITE 
function taconite($eval="", $replace="", $select=""){
	header('Content-type: text/xml'); 
	$str = '<taconite>
	';
	if($replace){
		$str .= "<replaceAndFadeIn select=\"$select\"><![CDATA[\n$replace\n]]>\n</replaceAndFadeIn>\n";
	}

	if($eval){
		$str .= "<eval><![CDATA[\n$eval\n]]>\n</eval>\n";
	}
	$str .= "\n</taconite>\n";
	echo $str;
}

//==================================================//

// Get browser
function getBrowser(){

$agent = $_SERVER['HTTP_USER_AGENT'];

if (eregi("opera",$agent)){
$val = explode(" ",stristr($agent,"opera"));
$bd['browser'] = $val[0];
$bd['version'] = $val[1];
}elseif(eregi("msie",$agent) && !eregi("opera",$agent) ){
$val = explode(" ",stristr($agent,"msie"));
$bd['browser'] = "Internet Explorer";
$bd['version'] = $val[1];
}elseif(eregi("safari",$agent) && !eregi("chrome",$agent)){
$val = explode(" ",stristr($agent,"Version"));
$val = explode("/",$val[0]);
$bd['browser'] = "Safari";
$bd['version'] = $val[1];
}elseif(eregi("firefox",$agent)){
$val = explode(" ",stristr($agent,"firefox"));
$val = explode("/",$val[0]);
$bd['browser'] = "Firefox";
$bd['version'] = $val[1];
}elseif(eregi("chrome",$agent)){
$val = explode(" ",stristr($agent,"chrome"));
$val = explode("/",$val[0]);
$bd['browser'] = "Chrome";
$bd['version'] = $val[1];
}else{
$bd['browser'] = "Unknown";
$bd['version'] = "0.0";
}

$bd['version'] = str_replace(";","",$bd['version']);

$browser = $bd['browser']." v".$bd['version'];

return $browser;

}

//==================================================//

// Get platform
function getPlatform(){

$platforms = "Mac,Linux,Windows";
$p = explode(",",$platforms);

foreach ( $p as $item )
{
	if(eregi($item,$_SERVER['HTTP_USER_AGENT'])){
		$platform = $item;
	}
}

return  $platform;

}


//=====================================================//
// ! CHECK REQUIRED INPUT FOR NON-AJAX
//=====================================================//

if($required_fields){
	$rf = explode(",", $required_fields);

	foreach ( $rf as $f )
	{    	
    	if(!$_POST[$f] && $f) {   	
    		if(!array_key_exists($f, $_FILES)) {  // See if we have a required file input.  	
    			$error = $f."+required+field+empty" ; 
    		}    	
    	}
	}
	
	// Validate email addresses
	if($_POST['email']){
		if (!eregi ("^([a-z0-9_]|\-|\.)+@(([a-z0-9_]|\-)+\.)+[a-z]{2,4}$", $_POST['email'])) {
			$err = urlencode(strip_tags($invalidEmailAlert));
			header("Location: $errorUrl?error=$err");die;
		}
	}
	
	// send error
	if ($error) {    
		if ($isAjax || $useAjax){
			$eval= "ydfl.blockUI({ message: '<img src=\"./".$filesFolder."/alert.png\" alt=\"Alert\" /><p>".$emptyRequiredFieldAlert."</p>".$okButton."' });
			ydfl('#block-button', '.blockMsg').click(function() { 
			ydfl.unblockUI(); 
			Recaptcha.reload(); });";
			taconite($eval, "", "");
		}else{			
			$err = urlencode(strip_tags($emptyRequiredFieldAlert));
			header("Location: $errorUrl?error=$error");die;
		}	
		
		exit();
	}

}


//=====================================================//
// ! CONVERT FIELDS
//=====================================================//


foreach ($fields as $index => $field)
{
	if($index != "mode" 
		&& $index != "submit" 
		&& $index != "Submit" 
		&& $index != "SUBMIT" 
		&& $index != "SUBMIT" 
		&& $index != "recaptcha_challenge_field" 
		&& $index != "recaptcha_response_field"
		&& $index != "ajax"){
		
		// to accomodate multi-select field
		if( is_array($field) ){
			$field = implode("," , $field);
		}
		
		
		$form_fields .= "<tr><td class=\"title\">".strtoupper($index).":</td><td>".nl2br($field)."</td></tr>\n";
				
	}
}


//=====================================================//
// ! SECURITY & VALIDATION
//=====================================================//

# was there a reCAPTCHA response?
if ($use_captcha == "true") {

	// Get a key from http://recaptcha.net/api/getkey
	$publickey = $reCaptchaPublicKey;
	$privatekey = $reCaptchaPrivateKey;

	# the response from reCAPTCHA
	$resp = null;
	# the error code from reCAPTCHA, if any
	$error = null;


       $resp = recaptcha_check_answer ($privatekey,
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);

        if (!$resp->is_valid) {
                # set the error code so that we can display it
                $error = $resp->error;				
        }

// send error
if ($error) {    
	if ($isAjax || $useAjax){
		$eval= "ydfl.blockUI({ message: '<img src=\"./".$filesFolder."/alert.png\" alt=\"Alert\" /><p>".$captchaFailed."</p>".$okButton."' });
		ydfl('#block-button', '.blockMsg').click(function() { 
		ydfl.unblockUI(); 
		Recaptcha.reload(); });";
		taconite($eval, "", "");
	}else{
		header("Location: $errorUrl?error=Security+Test+Failed");
	}	
    exit();
  }
  
          

}

//=====================================================//
// ! FILTER INPUT FOR HACK ATTEMPTS 
//=====================================================//

if ( !isset($email_bad_array) ) {
  $email_bad_array = "\r|\n|to:|cc:|bcc:";
}

// recipient
$sendToEmail = eregi_replace($email_bad_array,'',$sendToEmail);
$sendToEmail = str_replace(" ", "", $sendToEmail);

// cc
if ( !isset($cc) ) {
  $cc = "";
} else {
  $cc = eregi_replace($email_bad_array,'',$cc);
  $cc = str_replace(";", ",", $cc);
  $cc = str_replace(" ", "", $cc);
}

//bcc
if ( !isset($bcc) ) {
  $bcc = '';
} else {
  $bcc = eregi_replace($email_bad_array,'',$bcc);
  $bcc = str_replace(";", ",", $bcc);
  $bcc = str_replace(" ", "", $bcc);
}

// subject
if ( !isset($subject) ) {
  $subject = 'Form Submission' . ' from: ' . $_SERVER['HTTP_HOST'];
} else {
  $subject = eregi_replace($email_bad_array,'',$subject);
  $subject = stripslashes($subject);
}


//=====================================================//
// ! ATTACHMENTS
//=====================================================//
if ($_FILES) {
$files=array();
foreach ( $_FILES as $key=>$value )
{
  // code for file uploaded by form
  $attachment_name = $_FILES[$key]["name"];
  $attachment_size = $_FILES[$key]["size"];
  $attachment_temp = $_FILES[$key]["tmp_name"];
  $attachment_type = $_FILES[$key]["type"];
  $attachment_ext  = explode('.', $attachment_name);  
  $attachment_ext  = $attachment_ext[count($attachment_ext)-1];  
  if ( trim($attachment_temp) != '' && stristr($allowedFileTypes, $attachment_ext) != FALSE ) {

    if ($attachment_name) {
      if ($attachment_size > 0) {
	  
		// See is file is not too big.
		if($attachment_size > $maxAttachmentSizeInBytes){
		
			// send error   
			if ($isAjax || $useAjax){
				$eval= "ydfl.blockUI({ message: '<img src=\"./".$filesFolder."/alert.png\" alt=\"Alert\" /><p>".$fileTooBig."</p>".$okButton."' });
				ydfl('#block-button', '.blockMsg').click(function() { 
				ydfl.unblockUI(); 
				Recaptcha.reload(); });";
				taconite($eval, "", "");
			}else{
				header("Location: $errorUrl?error=Security+Test+Failed");
			}	
			exit();
		
		}
	  	  
        if (!$attachment_type) {
          $attachment_type =  "application/unknown";
        }
        $content    .= "Attached File: ".$attachment_name."<br />\n";
        $files[] = array('temp'=> $attachment_temp, 'name' => $attachment_name);
        
      }
    }
  }else{
  	$content .= "Non-Attached File: ".$attachment_name." (REJECTED)<br />\n";
  }
  
}//for

}



//=====================================================//
// ! SAVE TO MYSQL
//=====================================================//

if($saveToMySQL == "true"){
	
	// Connect to Database
	$s = @mysql_connect($mysqlHostName, $mysqlUserName, $mysqlPassword);
	$d = @mysql_select_db($mysqlDatabaseName, $s);
		
	if(!$s || !$d){ 		
		if($isAjax || $useAjax){
			$eval= "ydfl.blockUI({ message: '<img src=\"./".$filesFolder."/alert.png\" alt=\"Alert\" /><h1>Error!</h1><p>Database Connection Failed.</p>".$okButton."' });
			ydfl('#block-button', '.blockMsg').click(function() { 
			ydfl.unblockUI();
			Recaptcha.reload();  
			return false; });";
			taconite($eval, "", "");
		}else{
			header("Location: $errorUrl?error=".urlencode("Cannot Connect to Database"));		
		}		
	exit;	
	}
	
	// Save data
	$mySQLQuery = "INSERT INTO  SET ,  = '" . $_SERVER['REMOTE_ADDR'] . "',  = '" . $_SERVER['HTTP_USER_AGENT'] . "',  = NOW() ";
	$rs = @mysql_query($mySQLQuery);
	
	// Add returning MySQL errors
	
	if( mysql_errno() ){ 		
		if($isAjax || $useAjax){
			$eval= "ydfl.blockUI({ message: '<img src=\"./".$filesFolder."/alert.png\" alt=\"Alert\" /><h1>MySQL Error!</h1><p>".mysql_errno().": ".addslashes(mysql_error())."<br />When executing:<br />".addslashes($mySQLQuery)."</p>".$okButton."' });
			ydfl('#block-button', '.blockMsg').click(function() { 
			ydfl.unblockUI();
			Recaptcha.reload();  
			return false; });";
			taconite($eval, "", "");
		}else{
			header("Location: $errorUrl?error=".urlencode("mysql insert error"));		
		}		
	exit;	
	}
	
	
	

}

//=====================================================//
// ! CUSTOM CODE
//=====================================================//



//=====================================================//
// ! CREATE MESSAGE BODY 
//=====================================================//

// HTML VERSION //--------------------------------------

$htmlbody = "<html>
<head>
<title></title>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<style type=\"text/css\">
<!--
body , td{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #000000;
}
body {
	background-color: #f1f1f1;
}

h1{ font-size:16px; }

#formloom{
padding:10px;
background:#FFF;
border:1px solid #999;
}

td{ padding:4px; }

td.title{
font-weight:bold;
text-align:right;
}


-->
</style>
</head>

<body>



";

if(!$emailTemplate){


$htmlbody .= "<div id=\"formloom\">

<h1>".strtoupper($subject).":</h1>\n";
$htmlbody .= "<p><label>Date:</label>$today</p>\n";
$htmlbody .= "<div style=\"\">\n";
$htmlbody .= "<table><tbody>";
$htmlbody .= $form_fields;

if($content){ $htmlbody .= "<tr><td class=\"title\">Attachments:</td><td>$content</td></tr>"; }

$htmlbody .= "</tbody></table></div>";


}else{

$htmlbody .= $emailTemplate;

}


$htmlbody .= "</div>\n</body>\n</html>";

$htmlbody = stripslashes($htmlbody); // add this in case magic quotes are on.
	

//=====================================================//
// ! SEND MESSAGE 
//=====================================================//

$mail = new PHPMailer();
$mail->CharSet = "utf-8";

if($sendUsingToAddress == 'true'){

	$mail->From     = $sendToEmail;
	$mail->FromName = $sendToName;
	$mail->Sender = $sendToName; // added 02-04-10 09.02.13 AM - MY
	
	if($makeSenderReplyToAddress == 'true')
		{
			$mail->AddReplyTo($fromEmail,$fromName);
		}
}else{
	$mail->From     = $fromEmail;
	$mail->FromName = $fromName;
	$mail->Sender =  $fromEmail; // sets Return-Path
}

if($useSmtpServer){
	$mail->Mailer = "smtp";
	
	// added in 1.3.1
	if($smtpUsername){
		$mail->Username = $smtpUsername;
		$mail->Password = $smtpPassword;
		$mail->SMTPAuth = true;
	}
	
	// added in 1.3.1
	if($smtpSecure == "true"){
		$mail->SMTPSecure = "ssl";
	}
	
	$mail->Host = $smtpServerName.":".$smtpServerPort;
}



$mail->AddAddress($sendToEmail,$sendToName);


// add CC
if($cc){ 
	$ccs = explode(",", $cc);
	for ( $i = 0; $i < count($ccs); $i++ )
	{
    	$mail->addCC($ccs[$i]);
	}
}
// add BCC
if($bcc){ 
	$bccs = explode(",", $bcc);
	for ( $i = 0; $i < count($bccs); $i++ )
	{
    	$mail->addBCC($bccs[$i]);
	}
}

$textbody = trim( strip_tags( str_replace("<br />", "\n", $htmlbody) ) );

// Decide email format
if($usePlainText){
	$mail->Body=$textbody;
}else{
	$mail->IsHTML(true);
	//$mail->Encoding="7-bit";
	$mail->msgHTML($htmlbody);
	$mail->AltBody = $textbody;
}


$mail->Subject  =  $subject;
if($files){
	for ( $i = 0; $i < sizeof ( $files ); $i++ )
	{
 		$mail->AddAttachment($files[$i]['temp'], $files[$i]['name']);   
	}
}

if(!$mail->Send())
{
   	if($isAjax || $useAjax){
		$eval= "ydfl.blockUI({ message: '<img src=\"./".$filesFolder."/alert.png\" alt=\"Alert\" /><p>" . $mail->ErrorInfo . "</p>".$okButton."' });
		ydfl('#block-button', '.blockMsg').click(function() { 
		ydfl.unblockUI(); 
		return false; });";
		taconite($eval, "", "");
    }else{
		header("Location: $errorUrl?error=".urlencode($mail->ErrorInfo));
	}
	
    exit();
}

// Clear all addresses and attachments for next loop  
$mail->ClearAllRecipients();
$mail->ClearAttachments();
	

//=====================================================//
// ! SEND RECEIPT 
//=====================================================//

if($sendreceipt)
{
	
	
	// Add default receipt text if none was provided.
	if( !$receipt_text ){ $receipt_text = "Thank you for your submission."; }
	
	
	$mail->From = $sendToEmail;
	$mail->FromName = $sendToName;
	
	if($useSmtpServer){
		$mail->Mailer = "smtp";
		$mail->Username = $smtpUsername;
		$mail->Password = $smtpPassword;
		$mail->Host = $smtpServerName.":".$smtpServerPort;
	}

	$mail->AddAddress($fromEmail, $fromName);
	$mail->Sender =  $fromEmail; // sets Return-Path
	$mail->WordWrap = $wordwrap;                // set word wrap to 50 characters
	
	
	
	
	// Decide email format
	if($usePlainText){
		$mail->Body = trim( strip_tags( str_replace("<br />", "\n", $receipt_text) ) );
	}else{
		$mail->IsHTML(true);
		//$mail->Encoding="7-bit";
		$mail->msgHTML($receipt_text);
		$mail->AltBody = trim( strip_tags( str_replace("<br />", "\n", $receipt_text) ) );
	}
	
	
	$mail->Subject = "Re: $subject";

	if(!$mail->Send())
	{   
	if($isAjax || $useAjax){
		$eval= "ydfl.blockUI({ message: '<img src=\"./".$filesFolder."/alert.png\" alt=\"Alert\" /><p>" . $mail->ErrorInfo . "</p>".$okButton."' });
		ydfl('#block-button', '.blockMsg').click(function() { 
		ydfl.unblockUI(); 
		return false; });";
		taconite($eval, "", "");
    }else{
		header("Location: $errorUrl?error=".urlencode($mail->ErrorInfo));		
	}
    exit();
	}

} // END OF RECEIPT CHECK

if($isAjax || $useAjax){
	$replace =  $successLongMessage;
	$eval =" ydfl.blockUI({ message: '<img src=\"./".$filesFolder."/check.png\" alt=\"Success\" /><p>".addslashes($successMessage)."</p>' });
        ydfl('#formloom-form').resetForm();
        setTimeout(function() { ydfl.unblockUI(); }, 2000);";
	taconite($eval, $replace, "#formloom");
}else{
	header("Location: $successUrl");
}






?>
